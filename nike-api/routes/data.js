var express = require('express');
var router = express.Router();
const fs = require('fs');
const path = require('path');

router.get('/', async function (req, res, next) {
  const data = await JSON.parse(await fs.promises.readFile(path.resolve("inventory.json")))
  res.send(data)
});

router.post('/', async function (req, res) {
  const data = await JSON.parse(await fs.promises.readFile(path.resolve("inventory.json")))
  const updatedData = [...data, req.body]
  fs.writeFile(path.resolve("inventory.json"), JSON.stringify(updatedData), "utf8", (err) => {
    if (err) {
      console.log(err)
    } else {
      res.send("success")
    }
  })
})

module.exports = router;