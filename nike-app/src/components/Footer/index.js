import { useState } from "react";
import "./index.css";

function Footer() {
  const [showHelp, updateShowHelp] = useState(false);
  const [showAbout, updateShowAbout] = useState(false);

  return (
    <div id="Footer">
      <ul className="links">
        <li className="list">FIND A STORE</li>
        <li className="list">BECOME A MEMBER</li>
        <li className="list">SIGN UP FOR EMAIL</li>
        <li className="list">STUDENT DISCOUNTS</li>
        <li className="list">SEND US FEEDBACK</li>
      </ul>
      <button className="list" onClick={()=>updateShowHelp(!showHelp)}>
        GET HELP
        <span>{showHelp ? <img src="icons/minus.svg" alt="minus"/>:<img src="icons/plus.svg" alt="plus"/>}</span>
      </button>
      {showHelp && <ul className="hidden-list">
          <li>Order Status</li>
          <li>Delivery</li>
          <li>Returns</li>
          <li>Payment Options</li>
          <li>Contact Us on Nike.com Inquires</li>
      </ul>}
      <div className="empty"></div>
      <button className="list" onClick={()=>updateShowAbout(!showAbout)}>
        About Nike
        <span>{showAbout ? <img src="icons/minus.svg" alt="minus"/>:<img src="icons/plus.svg" alt="plus"/>}</span>
      </button>
     {showAbout &&  <ul className="hidden-list">
          <li>News</li>
          <li>Careers</li>
          <li>Investors</li>
          <li>Sustainability</li>
      </ul>}
    </div>
  );
}

export default Footer;
