import { Link } from "react-router-dom";
import "./index.css";

function JordanNav() {
    return (<header id="JordanNav">
        <Link to="/">
            <span>
                <img src="https://www.nike.com/assets/experience/ciclp/landing-pages/static/v2/243-3ff7573dc9d/static/icons/jordan.svg" alt="jordan-logo" className="logo" />
            </span>
        </Link>
        <nav>
            <Link to="/help" className="nav-Link">Help</Link>
            <span>|</span>
            <Link to="/register" className="nav-Link">Join Us</Link>
            <span>|</span>
            <Link to="/signin" className="nav-Link">Sign In</Link>
        </nav>
    </header>);
}

export default JordanNav;