import { Component } from "react";
import { Link } from "react-router-dom"
import JordanNav from '../JordanNav'
import "./index.css"

const regExp = RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/) // eslint-disable-line

class SigninRoute extends Component {
    constructor(props) {
        super(props);
        this.state = { email: "", password: "", keepMeSignedIn: false, isError: { email: "", password: "" } }
    }

    formValid = ({ isError, ...rest }) => {
        let isValid = false;
        Object.values(isError).forEach(val => {
            if (val.length > 0) {
                isValid = false
            } else {
                isValid = true
            }
        });
        Object.values(rest).forEach(val => {
            if (val === null) {
                isValid = false
            } else {
                isValid = true
            }
        });
        return isValid;
    };

    onInputChange = (event) => {
        const { name, value } = event.target;
        let isError = { ...this.state.isError };
        switch (name) {
            case "email":
                isError.email = regExp.test(value)
                    ? ""
                    : "Please enter a valid email address.";
                break;
            case "password":
                isError.password =
                    value.length === 0 ? "Please enter a password." : ""
                break;
            default:
                break;
        }
        this.setState({ isError, [name]: value })
    }

    render() {
        const { isError } = this.state
        return (<>
            <JordanNav />
            <div id="signin-container">
                <form className="sigin-form">
                    <img src="https://s3.nikecdn.com/unite/app/953/images/swoosh_black_2x.png" className="nike-logo" alt="nike-logo" />
                    <h1 >YOUR ACCOUNT FOR EVERYTHING NIKE</h1>
                    <input type="email"
                        name="email"
                        placeholder="Email address"
                        value={this.state.email}
                        onChange={this.onInputChange}
                        onBlur={this.onInputChange}
                        className={isError.email.length > 0 ? "is-invalid" : ""}
                    />
                    {isError.email.length > 0 && (<span className="error">{isError.email}</span>)}
                    <input type="password"
                        name="password"
                        placeholder="Password"
                        value={this.state.password}
                        onChange={this.onInputChange}
                        onBlur={this.onInputChange}
                        className={isError.password.length > 0 ? "is-invalid" : ""}
                    />
                    {isError.password.length > 0 && (<span className="error">{isError.password}</span>)}
                    <div className="keep-me-signedIn-container">
                        <input id="keep-me-signedIn" type="checkbox" />
                        <label htmlFor="keep-me-signedIn" className="keep-me-signedIn-text">Keep me signed in</label>
                    </div>
                    <div className="forgot-pwd-container">
                        <span className="forgot-pwd-text">
                            Forgot your password?
                        </span>
                    </div>
                    <p>By logging in, you agree to Nike's <Link to="/privacypolicy" className="link">Privacy Policy</Link> and <Link to="/termsofuse" className="link">Terms of Use</Link>.</p>
                    <button type="button">SIGN IN</button>
                    <p>Not a Member? <Link className="join-us" to="/register">Join Us.</Link></p>
                </form>
            </div>
        </>);
    }
}

export default SigninRoute;