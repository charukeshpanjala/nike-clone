import { Component } from "react";
import JordanNav from "../JordanNav";
import Footer from "../Footer";
import "./index.css";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div id="Home">
        <JordanNav />
        <div className="banner">
          <p>Save Upto 40%</p>
          {/* <div>
            <video controls autoplay="">
              <source src="videos/nike1.mp4" type="video/mp4" />
            </video>
          </div> */}
        </div>
        <div className="home-container">
          <div className="image-container">
            <img
              className="banner-image"
              src="https://static.nike.com/a/images/f_auto,cs_srgb/w_1920,c_limit/b53849bc-3468-467c-a069-e13d6c9adfd2/image.jpg"
              alt="banner"
            />
          </div>
          <div>
            <h4>
              THINK YOU'VE <br />
              SEEN IT ALL?
            </h4>
            <p className="desc-1">
              The first 50 years were just the beginning. Watch our 50th
              anniversary film,
              <br />
              directed by and starring Spike Lee and Indigo Hubbard-Salk.
            </p>
          </div>
          <h1>Featured</h1>
          <img
            className="banner-image"
            src="https://cf-images.us-east-1.prod.boltdns.net/v1/jit/72451143001/1b5e58b3-acaa-4116-99d3-9ebd2ff0a54f/main/1808x700/4s260ms/match/image.jpg"
            alt="featured"
          />
          <div>
            <h4>
              BREAKING <br /> BARRIERS <br />
              CHALLENGE
            </h4>
            <p className="desc-1">
              50 years and we are never done— run with us for the future of
              sport.
            </p>
            <button className="download-btn">Download the NRC App</button>
          </div>

        </div>
        <Footer />
      </div>
    );
  }
}

export default Home;
