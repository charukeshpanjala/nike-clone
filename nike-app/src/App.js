import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from './components/Home'
import SigninRoute from './components/SigninRoute'
import RegisterRoute from './components/RegisterRoute'
import TermsOfUsePage from './components/TermsOfUse';
import './App.css';


function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/signin" element={<SigninRoute />} />
        <Route path="/register" element={<RegisterRoute />} />
        <Route path="/termsofuse" element={<TermsOfUsePage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
