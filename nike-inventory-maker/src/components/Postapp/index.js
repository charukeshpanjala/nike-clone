import { Component } from 'react'
import "./index.css"

class Postapp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            uniqueId: "",
            brand: '',
            category1: "",
            category2: "",
            category3: "",
            category4: "",
            category5: "",
            category6: "",
            color: "",
            fullPrice: "",
            discount: "",
            gender: "",
            onsale: false,
            size: "",
            technology: "",
            mainUrl: "",
            icon: "",
            height: "",
            material: "",
            sport: "",
            title: "",
            description: "",
            sizeAndFit: "",
            Img1: "",
            Img2: "",
            Img3: "",
            Img4: "",
            Img5: "",
            Img6: "",
            Img7: "",
            Img8: ""

        }
    }

    onChangeId = (event) => {
        this.setState({ uniqueId: event.target.value });
    }
    onChangeTitle = (event) => {
        this.setState({ title: event.target.value });
    }
    onChangeDescription = (event) => {
        this.setState({ description: event.target.value });
    }

    onChangeColor = (event) => {
        this.setState({ color: event.target.value });
    }

    onChangeFullPrice = (event) => {
        this.setState({ fullPrice: event.target.value })
    }


    onChangeDiscount = (event) => {
        this.setState({ discount: event.target.value })
    }

    onChangeMainUrl = (event) => {
        this.setState({ mainUrl: event.target.value })
    }
    onChangesizeAndFit = (event) => {
        this.setState({ sizeAndFit: event.target.value })
    }

    onChangeBrand = (event) => {
        this.setState({ brand: event.target.value })
    }

    onChangeGender = (event) => {
        this.setState({ gender: event.target.value })
    }

    onChangeonSale = (event) => {
        this.setState({ onsale: event.target.value })
    }

    onChangeTechnology = (event) => {
        this.setState({ technology: event.target.value })
    }

    onChangeIcon = (event) => {
        this.setState({ icon: event.target.valueI })
    }

    onChangeMaterial = (event) => {
        this.setState({ material: event.target.value })
    }

    onChangeHeight = (event) => {
        this.setState({ height: event.target.value })
    }

    onChangeSport = (event) => {
        this.setState({ sport: event.target.value })
    }

    onChangeCategory1 = (event) => {
        this.setState({ category1: event.target.value })
    }

    onChangeCategory2 = (event) => {
        this.setState({ category2: event.target.value })
    }

    onChangeCategory3 = (event) => {
        this.setState({ category3: event.target.value })
    }

    onChangeCategory4 = (event) => {
        this.setState({ category4: event.target.value })
    }
    onChangeCategory5 = (event) => {
        this.setState({ category5: event.target.value })
    }
    onChangeCategory6 = (event) => {
        this.setState({ category6: event.target.value })
    }

    onChangeSize = (event) => {
        this.setState({ size: event.target.value })
    }

    onChangeImg1 = (event) => {
        this.setState({ Img1: event.target.value })
    }

    onChangeImg2 = (event) => {
        this.setState({ Img2: event.target.value })
    }
    onChangeImg3 = (event) => {
        this.setState({ Img3: event.target.value })
    }
    onChangeImg4 = (event) => {
        this.setState({ Img4: event.target.value })
    }
    onChangeImg5 = (event) => {
        this.setState({ Img5: event.target.value })
    }
    onChangeImg6 = (event) => {
        this.setState({ Img6: event.target.value })
    }
    onChangeImg7 = (event) => {
        this.setState({ Img7: event.target.value })
    }
    onChangeImg8 = (event) => {
        this.setState({ Img8: event.target.value })
    }

    onSubmitForm = async (event) => {
        event.preventDefault()

        const eachItemData = JSON.stringify(Object.keys(this.state).reduce((acc, eachValue) => {
            if (eachValue.includes("category")) {
                if (this.state[eachValue] !== "") {
                    if (acc["category"]) {
                        acc["category"] = [...acc["category"], this.state[eachValue]]
                    } else {
                        acc["category"] = [this.state[eachValue]]
                    }
                }
            }
            else if (eachValue.includes("Img")) {
                if (this.state[eachValue] !== "") {
                    if (acc["imageUrls"]) {
                        acc["imageUrls"] = [...acc["imageUrls"], this.state[eachValue]]
                    } else {
                        acc["imageUrls"] = [this.state[eachValue]]
                    }
                }
            }
            else {
                acc[eachValue] = this.state[eachValue]
            }
            return acc
        }, {}))
        await fetch("http://localhost:5000/data", {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: eachItemData
        })
        alert("Item psoted successfully");
    }

    render() {
        return (<div className="postapp">
            <form className="form" onSubmit={this.onSubmitForm}>
                <div className="label-container">
                    <label className="label-name">id</label>
                    <input type="text" className="label-input" value={this.state.uniqueId} onChange={this.onChangeId} />
                </div>
                <div className="label-container">
                    <label className="label-name">Title</label>
                    <textarea type="text" rows="2" className="label-input" value={this.state.title} onChange={this.onChangeTitle}></textarea>
                </div>
                <div className="label-container">
                    <label className="label-name">Description</label>
                    <textarea type="text" rows="3" className="label-input" value={this.state.description} onChange={this.onChangeDescription}></textarea>
                </div>
                <div className="label-container">
                    <label className="label-name">color</label>
                    <input type="text" className="label-input" value={this.state.color} onChange={this.onChangeColor} />
                </div>
                <div className="label-container">
                    <label className="label-name">FullPrice</label>
                    <input type="text" className="label-input" value={this.state.fullPrice} onChange={this.onChangeFullPrice} />
                </div>
                <div className="label-container">
                    <label className="label-name">Discount in %</label>
                    <input type="text" className="label-input" value={this.state.discount} onChange={this.onChangeDiscount} />
                </div>
                <div className="label-container">
                    <label className="label-name">sizeAndFit</label>
                    <input type="text" className="label-input" value={this.state.sizeAndFit} onChange={this.onChangesizeAndFit} />
                </div>
                <div className="label-container">
                    <label className="label-name">Brand</label>
                    <select className="label-input" value={this.state.brand} onChange={this.onChangeBrand}>
                        <option>select</option>
                        <option>nike</option>
                        <option>jordan</option>
                    </select>
                </div>
                <div className="label-container">
                    <label className="label-name">Gender</label>
                    <select className="label-input" value={this.state.gender} onChange={this.onChangeGender}>
                        <option>select</option>
                        <option>male</option>
                        <option>female</option>
                        <option>unisex</option>
                    </select>
                </div>
                <div className="label-container">
                    <label className="label-name">Sale</label>
                    <select className="label-input" value={this.state.onsale} onChange={this.onChangeonSale}>
                        <option>false</option>
                        <option>true</option>
                    </select>
                </div>
                <div className="label-container">
                    <label className="label-name">technology</label>
                    <select className="label-input" value={this.state.technology} onChange={this.onChangeTechnology}>
                        <option>select</option>
                        <option>Nike Air</option>
                        <option>Nike Max Air</option>
                        <option>Nike React</option>
                        <option>Nike Zoom Air</option>
                        <option>Dri-Fit</option>
                        <option>Therma-Fit</option>
                        <option>Storm-Fit</option>
                    </select>
                </div>
                <div className="label-container">
                    <label className="label-name">Icon</label>
                    <select className="label-input" value={this.state.icon} onChange={this.onChangeIcon}>
                        <option>select</option>
                        <option>Air Max</option>
                        <option>Blazer</option>
                        <option>Air Force 1</option>
                        <option>Air Jordan 1</option>
                    </select>
                </div>
                <div className="label-container">
                    <label className="label-name">Material</label>
                    <select className="label-input" value={this.state.material} onChange={this.onChangeMaterial}>
                        <option>select</option>
                        <option>Canvas</option>
                        <option>Leather</option>
                        <option>Synthetics</option>
                        <option>Fleece</option>
                        <option>Tech Fleece</option>
                        <option>Sustainable Materials</option>
                    </select>
                </div>
                <div className="label-container">
                    <label className="label-name">height</label>
                    <select className="label-input" value={this.state.height} onChange={this.onChangeHeight}>
                        <option>select</option>
                        <option>Low-top</option>
                        <option>Mid-top</option>
                        <option>High-top</option>
                    </select>
                </div>
                <div className="label-container">
                    <label className="label-name">Sport</label>
                    <select className="label-input" value={this.state.sport} onChange={this.onChangeSport}>
                        <option>Select</option>
                        <option>Running</option>
                        <option>Football</option>
                        <option>Basketball</option>
                        <option>Tennis</option>
                        <option>Golf</option>
                        <option>Walking</option>
                    </select>
                </div>
                <div className="label-container">
                    <label className="label-name">category1</label>
                    <select className="label-input" value={this.state.category1} onChange={this.onChangeCategory1}>
                        <option>select</option>
                        <option>Shoes</option>
                        <option>New Releases</option>
                        <option>Basic Essentials</option>
                        <option>Football Club Kits</option>
                        <option>Newest Sneakers</option>
                        <option>Clothing</option>
                        <option>T-shirt</option>
                        <option>Hoodies</option>
                        <option>Sweatshirts</option>
                        <option>Caps</option>
                        <option>Shorts</option>
                        <option>Tracksuit</option>
                        <option>Jacket</option>
                        <option>Socks</option>
                        <option>Skirt</option>
                        <option>Dress</option>
                        <option>Sale</option>
                    </select>
                </div>
                <div className="label-container">
                    <label className="label-name">category2</label>
                    <select className="label-input" value={this.state.category2} onChange={this.onChangeCategory2}>
                        <option>select</option>
                        <option>Shoes</option>
                        <option>New Releases</option>
                        <option>Basic Essentials</option>
                        <option>Football Club Kits</option>
                        <option>Newest Sneakers</option>
                        <option>Clothing</option>
                        <option>T-shirt</option>
                        <option>Hoodies</option>
                        <option>Sweatshirts</option>
                        <option>Caps</option>
                        <option>Shorts</option>
                        <option>Tracksuit</option>
                        <option>Jacket</option>
                        <option>Socks</option>
                        <option>Skirt</option>
                        <option>Dress</option>
                        <option>Sale</option>
                    </select>
                </div>
                <div className="label-container">
                    <label className="label-name">category3</label>
                    <select className="label-input" value={this.state.category3} onChange={this.onChangeCategory3}>
                        <option>select</option>
                        <option>Shoes</option>
                        <option>New Releases</option>
                        <option>Basic Essentials</option>
                        <option>Football Club Kits</option>
                        <option>Newest Sneakers</option>
                        <option>Clothing</option>
                        <option>T-shirt</option>
                        <option>Hoodies</option>
                        <option>Sweatshirts</option>
                        <option>Caps</option>
                        <option>Shorts</option>
                        <option>Tracksuit</option>
                        <option>Jacket</option>
                        <option>Socks</option>
                        <option>Skirt</option>
                        <option>Dress</option>
                        <option>Sale</option>
                    </select>
                </div>
                <div className="label-container">
                    <label className="label-name">category4</label>
                    <select className="label-input" value={this.state.category4} onChange={this.onChangeCategory4}>
                        <option>select</option>
                        <option>Shoes</option>
                        <option>New Releases</option>
                        <option>Basic Essentials</option>
                        <option>Football Club Kits</option>
                        <option>Newest Sneakers</option>
                        <option>Clothing</option>
                        <option>T-shirt</option>
                        <option>Hoodies</option>
                        <option>Sweatshirts</option>
                        <option>Caps</option>
                        <option>Shorts</option>
                        <option>Tracksuit</option>
                        <option>Jacket</option>
                        <option>Socks</option>
                        <option>Skirt</option>
                        <option>Dress</option>
                        <option>Sale</option>
                    </select>
                </div>
                <div className="label-container">
                    <label className="label-name">category5</label>
                    <select className="label-input" value={this.state.category5} onChange={this.onChangeCategory5}>
                        <option>select</option>
                        <option>Shoes</option>
                        <option>New Releases</option>
                        <option>Basic Essentials</option>
                        <option>Football Club Kits</option>
                        <option>Newest Sneakers</option>
                        <option>Clothing</option>
                        <option>T-shirt</option>
                        <option>Hoodies</option>
                        <option>Sweatshirts</option>
                        <option>Caps</option>
                        <option>Shorts</option>
                        <option>Tracksuit</option>
                        <option>Jacket</option>
                        <option>Socks</option>
                        <option>Skirt</option>
                        <option>Dress</option>
                        <option>Sale</option>
                    </select>
                </div>
                <div className="label-container">
                    <label className="label-name">category6</label>
                    <select className="label-input" value={this.state.category6} onChange={this.onChangeCategory6}>
                        <option>select</option>
                        <option>Shoes</option>
                        <option>New Releases</option>
                        <option>Basic Essentials</option>
                        <option>Football Club Kits</option>
                        <option>Newest Sneakers</option>
                        <option>Clothing</option>
                        <option>T-shirt</option>
                        <option>Hoodies</option>
                        <option>Sweatshirts</option>
                        <option>Caps</option>
                        <option>Shorts</option>
                        <option>Tracksuit</option>
                        <option>Jacket</option>
                        <option>Socks</option>
                        <option>Skirt</option>
                        <option>Dress</option>
                        <option>Sale</option>
                    </select>
                </div>
                <div className="label-container">
                    <label className="label-name">Size-type</label>
                    <select className="label-input" value={this.state.size} onChange={this.onChangeSize}>
                        <option>select</option>
                        <option>UK 5,UK 6.5,UK 7,UK 7.5,UK 8,UK 8.5,UK 9,UK 9.5, UK 10, UK 10.5, UK 11 </option>
                        <option>XS,S,M,L,XL,XXL</option>
                        <option>US XS,US S,US M,US L,US XL,US XXL</option>
                        <option>Onesize</option>
                    </select>
                </div>
                <div className="label-container">
                    <label className="label-name">Main Url</label>
                    <input type="text" className="label-input" value={this.state.mainUrl} onChange={this.onChangeMainUrl} />
                </div>
                <div className="label-container">
                    <label className="label-name">ImgUrl1</label>
                    <input type="text" className="label-input" value={this.state.Img1} onChange={this.onChangeImg1} />
                </div>
                <div className="label-container">
                    <label className="label-name">ImgUrl2</label>
                    <input type="text" className="label-input" value={this.state.Img2} onChange={this.onChangeImg2} />
                </div>
                <div className="label-container">
                    <label className="label-name">ImgUrl3</label>
                    <input type="text" className="label-input" value={this.state.Img3} onChange={this.onChangeImg3} />
                </div>
                <div className="label-container">
                    <label className="label-name">ImgUrl4</label>
                    <input type="text" className="label-input" value={this.state.Img4} onChange={this.onChangeImg4} />
                </div>
                <div className="label-container">
                    <label className="label-name">ImgUrl5</label>
                    <input type="text" className="label-input" value={this.state.Img5} onChange={this.onChangeImg5} />
                </div>
                <div className="label-container">
                    <label className="label-name">ImgUrl6</label>
                    <input type="text" className="label-input" value={this.state.Img6} onChange={this.onChangeImg6} />
                </div>
                <div className="label-container">
                    <label className="label-name">ImgUrl7</label>
                    <input type="text" className="label-input" value={this.state.Img7} onChange={this.onChangeImg7} />
                </div>
                <div className="label-container">
                    <label className="label-name">ImgUrl8</label>
                    <input type="text" className="label-input" value={this.state.Img8} onChange={this.onChangeImg8} />
                </div>

                <button type="Submit" className="btn">POST</button>
            </form>
        </div>);
    }
}

export default Postapp;