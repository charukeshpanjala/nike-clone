import Postapp from './components/Postapp'
import './App.css';

function App() {
  return (
    <div className="App-header">
      <Postapp />
    </div>
  );
}

export default App;
